///////////////////////////////////////////////////////////////////////////////
// LDD_Buyer.mq4
// RESULT: with this we have 2.5 dollars profit in a day
// DESCRIPT: this is the simplest version, everything complicated is in comment
///////////////////////////////////////////////////////////////////////////////


//Select the orders by tichet and not by order
// This way we do not get orders fron another currency

#property copyright "bartvertongen70@gmail.com"
#property link      "https://www.lddevelop.com"
#property version   "1.00"
#property description "With this version we loose money when the currency goes down"
#property strict

#define MAXLEVEL 3
extern string strSymbol = "EURUSD"; //What currency pair do we use now?
extern double dblLotSize = 0.1; //How much do we buy each time
extern double dblLevelLoss = -10.0; //How much lost before we buy again
extern double dblLevelProfit = 1.0; //How much profit do we want from a loosing lot?
double dblBuyStopLoss = 0.0;
double dblBuyTakeProfit = 0.0;

int fd = -1;   //File descriptor

//Long Positions
int arrBuyOrders[MAXLEVEL+1]; //Contains the tickets of the orders
int nCurrBuyTicket = -1;
bool bSkipBuy = false;
bool bBuyIsLoosing = true;  //we set true to start

//#define DEBUG

///////////////////////////////////////////////////////////////////////////////
int OnInit()
{
   ArrayFill(arrBuyOrders, 0, 4, -1);
   arrBuyOrders[0] = 0; //we use the first index to keep the size
   string LogFileName = "D:\\MyWork\\Forex-Trading\\"+"SimpleExpert"+"_"+Symbol()+".log";
   ResetLastError(); 
   fd = FileOpen(LogFileName, FILE_WRITE|FILE_TXT|FILE_UNICODE); 
   if (fd != INVALID_HANDLE) 
   { 
      FileWrite(fd, TimeCurrent(), Symbol(), EnumToString(ENUM_TIMEFRAMES(_Period)));        
      Print("FileOpen OK"); 
   } 
   else 
      Print("Operation FileOpen failed, error ",GetLastError()); 
   FileFlush(fd); 
   return(INIT_SUCCEEDED);
}

///////////////////////////////////////////////////////////////////////////////
void OnDeinit(const int reason)
{
   FileWrite(fd, TimeCurrent(), strSymbol, " We are in OnDeinit!");
   ArrayFree(arrBuyOrders);   //it is not really needed we leave anyway
   FileFlush(fd);
   FileClose(fd);
}


///////////////////////////////////////////////////////////////////////////////
// Expert tick function 
///////////////////////////////////////////////////////////////////////////////
int start() 
{
    double dblResult = 0.0; 
   
    //LONG POSITIONS
    //If we have no buy orders then we make one
    if (nCurrBuyTicket == -1) 
    {  
        CreateBuyOrder();    
    }
    else //if we have allready an order
    {
        FileWrite(fd, TimeCurrent(), " We have orders and we will check them.");
        FileFlush(fd);
        dblResult = CheckBuy();
    }
    return 0; //all ok}
}

///////////////////////////////////////////////////////////////////////////////
// Create a Long position
///////////////////////////////////////////////////////////////////////////////
void CreateBuyOrder()
{
    FileWrite(fd, TimeCurrent(), " We will make an Order.");
    int Err = 0;                     
    while (IsTradeContextBusy()) 
        Sleep(10);
    //get the latest ticks in the array buffer                     
    RefreshRates();        
    double ask   = MarketInfo(strSymbol, MODE_ASK); // Request for the value of Ask
    double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
    //Calculated SL and TP prices must be normalized
    dblBuyStopLoss = NormalizeDouble(ask-1.0*point,Digits);   
    dblBuyTakeProfit = NormalizeDouble(ask+5.0*point,Digits);                 
    if (nCurrBuyTicket == -1)
    {
        //for open buy ASK for close BID      
        nCurrBuyTicket = OrderSend(strSymbol, OP_BUY, dblLotSize, ask, 50, 0, 0);
        if (nCurrBuyTicket == -1)
            Comment("Err = " + string(GetLastError()));
    }
          
}


///////////////////////////////////////////////////////////////////////////////
// DESCRIPT: checks if there is profit and closes the LONG order if there is.
///////////////////////////////////////////////////////////////////////////////
double CheckBuy()
{  
    double dblProfit = 0;
    
    Comment("BEGIN CHECK");
    //First we select the current BuyTicket
    if (OrderSelect(nCurrBuyTicket, SELECT_BY_TICKET) == false)
      Comment("OrderSelect returned the error of " + string(GetLastError()));
    else
    {
        Comment("We are checking profit now");
        //Order profit can be negative or positive
        dblProfit = OrderProfit() - MathAbs(OrderSwap()) - MathAbs(OrderCommission());
        if (dblProfit > 0.0)
        {

            if (dblProfit > dblBuyTakeProfit)
            {  
                RefreshRates();
                double bid   = MarketInfo(strSymbol, MODE_BID); // Request for the value of Ask        
                double ask   = MarketInfo(strSymbol, MODE_ASK); // Request for the value of Ask
                double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
                //Calculated SL and TP prices must be normalized
                dblBuyStopLoss = NormalizeDouble(ask-1.0*point,Digits);   
                dblBuyTakeProfit = NormalizeDouble(ask+5.0*point,Digits);            
                //Close BUY to get the profit -- its important we use BID price to close
                if (OrderClose(nCurrBuyTicket, dblLotSize, bid, 1, Red) == false)
                    Comment("We could not close the current buy order with profit!");             
                else
                    nCurrBuyTicket = -1;                      
            }            
        }            
/*        else if (dblProfit <= dblLevelLoss) //if the loss is too high
        {
            if (arrBuyOrders[0] < MAXLEVEL) //we still have place
            {
               //We put the current ticket in the array of losing orders
               arrBuyOrders[arrBuyOrders[0]+1] = nCurrBuyTicket;
               arrBuyOrders[0]++;
               nCurrBuyTicket = -1;                   
            }
            else //if we are at the limit of loosing orders we remove one and add a new one
            {
                //We keep the last loosing order because it has the highest change
                //    of becoming profitable
                //We keep the first loosing order because it will make us loose the most.
                //So we close the second last order
                bool retval = OrderClose(arrBuyOrders[MAXLEVEL-1], dblLotSize, Ask, 1, Red);
                arrBuyOrders[MAXLEVEL-1] = arrBuyOrders[MAXLEVEL];
                arrBuyOrders[MAXLEVEL] = -1;
                arrBuyOrders[0]--;
                //We put the old ticket in the array of losing orders
                arrBuyOrders[arrBuyOrders[0]+1] = nCurrBuyTicket;
                arrBuyOrders[0]++;
                nCurrBuyTicket = -1;                      
            }        
            //Make a new Current buy order
            RefreshRates();        
            double bid   = MarketInfo(strSymbol ,MODE_BID); // Request for the value of Bid
            double ask   = MarketInfo(strSymbol, MODE_ASK); // Request for the value of Ask
            double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
            //Calculated SL and TP prices must be normalized                     
            nCurrBuyTicket = OrderSend(strSymbol, OP_BUY, 0.1, bid, 50, 0, 0);
            if (nCurrBuyTicket == -1)
               Comment("Err = ", GetLastError());
        } //end of IF loss is too high             
    }//End of what we do with the selected current order
    //Now we Check if one of the loosing orders has profit
    for(int j=1;j <= arrBuyOrders[0]; j++)
    {
      if (OrderSelect(arrBuyOrders[j], SELECT_BY_TICKET) == true)
      {
         dblProfit = OrderProfit() - MathAbs(OrderSwap()) 
                                    - MathAbs(OrderCommission());
         if (dblProfit > dblLevelProfit)
         {
            if (OrderClose(arrBuyOrders[j], dblLotSize, Ask, 1, Red) == false)
               Comment("the buy order with profit from losing list could not be closed");
            else
            {
                //adapt the array after selling
                for(int k=j;k < arrBuyOrders[0];k++)
                {
                   arrBuyOrders[k] = arrBuyOrders[k+1]; //shift the orders
                   arrBuyOrders[k+1] = -1;
                   arrBuyOrders[0]--;
                }
                
            }
            break; //if we found profit we leave the FOR loop
            //If others have profit we will process it at next tick
         }                                              
      }//End of an selected order
      else
         Comment("We could not select order with ticketnr = " + string(arrBuyOrders[j]));*/         
   }
   Comment("END CHECK");
   return(dblProfit);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////