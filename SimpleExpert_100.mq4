///////////////////////////////////////////////////////////////////////////////
// SimpleExpert.mq4
///////////////////////////////////////////////////////////////////////////////

// THERE IS A BUG
// If we arrive at the losslevel we have to put the old ticket in the array
// and keep the new one as current

//Select the orders by tichet and not by order
// This way we do not get orders fron another currency

#property copyright "bartvertongen70@gmail.com"
#property link      "https://www.lddevelop.com"
#property version   "1.00"
#property description "With this version we loose money when the currency goes down"
#property strict

#define MAXLEVEL 3
extern string strSymbol = "EURUSD"; //What currency pair do we use now?
extern double dblLotSize = 0.1; //How much do we buy each time
extern double dblLevelLoss = -10.0; //How much lost before we buy again
extern double dblLevelProfit = 1.0; //How much profit do we want from a loosing lot?
double dblBuyStopLoss = 0.0;
double dblBuyTakeProfit = 0.0;
double dblSellStopLoss = 0.0;
double dblSellTakeProfit = 0.0;
int fd = -1;   //File descriptor

//Long Positions
int arrBuyOrders[MAXLEVEL+1]; //Contains the tickets of the orders
int nCurrBuyTicket = -1;
bool bSkipBuy = false;
bool bSellIsLoosing = true; //we set true to start
bool bBuyIsLoosing = true;  //we set true to start

//Short Positions
int arrSellOrders[MAXLEVEL+1]; //Contains the tickets of the orders
int nCurrSellTicket = -1;
bool bSkipSell = false;
//#define DEBUG

///////////////////////////////////////////////////////////////////////////////
int OnInit()
{
   ArrayFill(arrBuyOrders, 0, 4, -1);
   ArrayFill(arrSellOrders, 0, 4, -1);
   arrBuyOrders[0] = 0; //we use the first index to keep the size
   arrSellOrders[0] = 0; //we use the first index to keep the size
   string LogFileName = "D:\\MyWork\\Forex-Trading\\"+"SimpleExpert"+"_"+Symbol()+".log";
   ResetLastError(); 
   fd = FileOpen(LogFileName, FILE_WRITE|FILE_TXT|FILE_UNICODE); 
   if (fd != INVALID_HANDLE) 
   { 
      FileWrite(fd, TimeCurrent(), Symbol(), EnumToString(ENUM_TIMEFRAMES(_Period)));        
      Print("FileOpen OK"); 
   } 
   else 
      Print("Operation FileOpen failed, error ",GetLastError()); 
   FileFlush(fd); 
   return(INIT_SUCCEEDED);
}

///////////////////////////////////////////////////////////////////////////////
void OnDeinit(const int reason)
{
   FileWrite(fd, TimeCurrent(), strSymbol, " We are in OnDeinit!");
   ArrayFree(arrBuyOrders);   //it is not really needed we leave anyway
   ArrayFree(arrSellOrders);   //it is not really needed we leave anyway
   FileFlush(fd);
   FileClose(fd);
}


///////////////////////////////////////////////////////////////////////////////
// Expert tick function 
///////////////////////////////////////////////////////////////////////////////
int start() 
{
    double dblResult = 0.0; 
   
    //LONG POSITIONS
    //If we have no buy orders then we make one
    if (nCurrBuyTicket == -1) 
    {  
        CreateBuyOrder();    
    }
    else //if we have allready an order
    {
        FileWrite(fd, TimeCurrent(), " We have orders and we will check them.");
        FileFlush(fd);
        dblResult = CheckBuy();
    }
    //SHORT POSITIONS
    if (nCurrSellTicket == -1) 
    {  
        CreateSellOrder();    
    }
    else //if we have allready an order
    {
        FileWrite(fd, TimeCurrent(), " We have orders and we will check them.");
        FileFlush(fd);
        dblResult = CheckSell();
    }    
    return 0; //all ok}
}

///////////////////////////////////////////////////////////////////////////////
// Create a Long position
///////////////////////////////////////////////////////////////////////////////
void CreateBuyOrder()
{
    FileWrite(fd, TimeCurrent(), " We will make an Order.");
    int Err = 0;                     
    while (IsTradeContextBusy()) 
        Sleep(10);
    //get the latest ticks in the array buffer                     
    RefreshRates();        
    double ask   = MarketInfo(strSymbol, MODE_ASK); // Request for the value of Ask
    double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
    //Calculated SL and TP prices must be normalized
    dblBuyStopLoss = NormalizeDouble(ask-1.0*point,Digits);   
    dblBuyTakeProfit = NormalizeDouble(ask+5.0*point,Digits);                 
    if (nCurrBuyTicket == -1 && bSellIsLoosing)
    {
        //for open buy ASK for close BID      
        nCurrBuyTicket = OrderSend(strSymbol, OP_BUY, dblLotSize, ask, 50, 0, 0);
        if (nCurrBuyTicket == -1)
            Comment("Err = " + string(GetLastError()));
    }
          
}

///////////////////////////////////////////////////////////////////////////////
// Create a Short position
///////////////////////////////////////////////////////////////////////////////
void CreateSellOrder()
{
    FileWrite(fd, TimeCurrent(), " We will make an Order.");
    int Err = 0;                     
    while (IsTradeContextBusy()) 
        Sleep(10);
    //get the latest ticks in the array buffer                     
    RefreshRates();        
    double bid   = MarketInfo(strSymbol ,MODE_BID); // Request for the value of Bid
    double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
    //Calculated SL and TP prices must be normalized
    dblSellStopLoss = NormalizeDouble(bid-1.0*point,Digits);   
    dblSellTakeProfit = NormalizeDouble(bid+5.0*point,Digits);                   
    if (nCurrSellTicket==-1 && bBuyIsLoosing)
    {
        //for open BID for close ASK      
        nCurrSellTicket = OrderSend(strSymbol, OP_SELL, dblLotSize, bid, 50, 0, 0);
        if (nCurrSellTicket == -1)
            Comment("Err = " + string(GetLastError()));
    }      
}

///////////////////////////////////////////////////////////////////////////////
// DESCRIPT: checks if there is profit and closes the LONG order if there is.
///////////////////////////////////////////////////////////////////////////////
double CheckBuy()
{
    //static double dblLastLostPrice = 0.0;   
    double dblProfit = 0;
    
    Comment("BEGIN CHECK");
    //First we select the current BuyTicket
    if (OrderSelect(nCurrBuyTicket, SELECT_BY_TICKET) == false)
      Comment("OrderSelect returned the error of " + string(GetLastError()));
    else
    {
        Comment("We are checking profit now");
        //Order profit can be negative or positive
        dblProfit = OrderProfit() - MathAbs(OrderSwap()) - MathAbs(OrderCommission());
        if (dblProfit > 0.0)
        {
            bBuyIsLoosing = false;
            bSellIsLoosing = true;
            if (dblProfit > 0.0/*dblBuyTakeProfit*/)
            {  
                //Close to get the profit
                if (OrderClose(nCurrBuyTicket, dblLotSize, Bid, 1, Red) == false)
                    Comment("We could not close the current buy order with profit!");             
                else
                    nCurrBuyTicket = -1;                         
            }            
        }            
        else
        {
            //Close to minimaze the loss
            if (OrderClose(nCurrBuyTicket, dblLotSize, Bid, 1, Red) == false)
                Comment("We could not close the current buy order with profit!");             
            else
                nCurrBuyTicket = -1;        
            bBuyIsLoosing = true;
            bSellIsLoosing = false;
        }            

/*        else if (dblProfit <= dblLevelLoss) //if the loss is too high
        {
            if (arrBuyOrders[0] < MAXLEVEL) //we still have place
            {
               //We put the current ticket in the array of losing orders
               arrBuyOrders[arrBuyOrders[0]+1] = nCurrBuyTicket;
               arrBuyOrders[0]++;
               nCurrBuyTicket = -1;                   
            }
            else //if we are at the limit of loosing orders we remove one and add a new one
            {
                //We keep the last loosing order because it has the highest change
                //    of becoming profitable
                //We keep the first loosing order because it will make us loose the most.
                //So we close the second last order
                bool retval = OrderClose(arrBuyOrders[MAXLEVEL-1], dblLotSize, Ask, 1, Red);
                arrBuyOrders[MAXLEVEL-1] = arrBuyOrders[MAXLEVEL];
                arrBuyOrders[MAXLEVEL] = -1;
                arrBuyOrders[0]--;
                //We put the old ticket in the array of losing orders
                arrBuyOrders[arrBuyOrders[0]+1] = nCurrBuyTicket;
                arrBuyOrders[0]++;
                nCurrBuyTicket = -1;                      
            }        
            //Make a new Current buy order
            RefreshRates();        
            double bid   = MarketInfo(strSymbol ,MODE_BID); // Request for the value of Bid
            double ask   = MarketInfo(strSymbol, MODE_ASK); // Request for the value of Ask
            double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
            //Calculated SL and TP prices must be normalized                     
            nCurrBuyTicket = OrderSend(strSymbol, OP_BUY, 0.1, bid, 50, 0, 0);
            if (nCurrBuyTicket == -1)
               Comment("Err = ", GetLastError());
        } //end of IF loss is too high             
    }//End of what we do with the selected current order
    //Now we Check if one of the loosing orders has profit
    for(int j=1;j <= arrBuyOrders[0]; j++)
    {
      if (OrderSelect(arrBuyOrders[j], SELECT_BY_TICKET) == true)
      {
         dblProfit = OrderProfit() - MathAbs(OrderSwap()) 
                                    - MathAbs(OrderCommission());
         if (dblProfit > dblLevelProfit)
         {
            if (OrderClose(arrBuyOrders[j], dblLotSize, Ask, 1, Red) == false)
               Comment("the buy order with profit from losing list could not be closed");
            else
            {
                //adapt the array after selling
                for(int k=j;k < arrBuyOrders[0];k++)
                {
                   arrBuyOrders[k] = arrBuyOrders[k+1]; //shift the orders
                   arrBuyOrders[k+1] = -1;
                   arrBuyOrders[0]--;
                }
                
            }
            break; //if we found profit we leave the FOR loop
            //If others have profit we will process it at next tick
         }                                              
      }//End of an selected order
      else
         Comment("We could not select order with ticketnr = " + string(arrBuyOrders[j]));*/         
   }
   Comment("END CHECK");
   return(dblProfit);
}

///////////////////////////////////////////////////////////////////////////////
// DESCRIPT: checks if there is profit and closes the SHORT order if there is.
///////////////////////////////////////////////////////////////////////////////
double CheckSell()
{
    //static double dblLastLostPrice = 0.0;   
    double dblProfit = 0;
    
    Comment("BEGIN CHECK SELL");
    //First we select the current BuyTicket
    if (OrderSelect(nCurrSellTicket, SELECT_BY_TICKET) == false)
      Comment("OrderSelect returned the error of " + string(GetLastError()));
    else
    {
        Comment("We are checking profit now");
        //Order profit can be negative or positive
        dblProfit = OrderProfit() - MathAbs(OrderSwap()) - MathAbs(OrderCommission());
        if (dblProfit > 0.0)
        {
            bSellIsLoosing = false;
            bBuyIsLoosing = true;
            if (dblProfit > 0.0/*dblSellTakeProfit*/)
            {  
                //Close to take the profit
                if (OrderClose(nCurrSellTicket, dblLotSize, Bid, 1, Red) == false)
                    Comment("We could not close the current sell order with profit!");             
                else
                    nCurrSellTicket = -1;                            
            }            
        }
        else
        {
            bSellIsLoosing = true;
            bBuyIsLoosing = false;
           //Close to minimalize the current loss
            if (OrderClose(nCurrSellTicket, dblLotSize, Ask, 1, Red) == false)
                Comment("We could not close the current sell order with loss!");             
            else
                nCurrSellTicket = -1;                
        }

        /*else if (dblProfit <= dblLevelLoss) //if the loss is too high
        {
            if (arrSellOrders[0] < MAXLEVEL) //we still have place
            {
               //We put the current ticket in the array of losing orders
               arrSellOrders[arrSellOrders[0]+1] = nCurrSellTicket;
               arrSellOrders[0]++;
               nCurrSellTicket = -1;                   
            }
            else //if we are at the limit of loosing orders we remove one and add a new one
            {
                //We keep the last loosing order because it has the highest change
                //    of becoming profitable
                //We keep the first loosing order because it will make us loose the most.
                //So we close the second last order
                bool retval = OrderClose(arrSellOrders[MAXLEVEL-1], dblLotSize, Ask, 1, Red);
                arrSellOrders[MAXLEVEL-1] = arrSellOrders[MAXLEVEL];
                arrSellOrders[MAXLEVEL] = -1;
                arrSellOrders[0]--;
                //We put the old ticket in the array of losing orders
                arrSellOrders[arrBuyOrders[0]+1] = nCurrSellTicket;
                arrSellOrders[0]++;
                nCurrSellTicket = -1;                      
            }        
            //Make a new Current Sell order
            RefreshRates();        
            double bid   = MarketInfo(strSymbol ,MODE_BID); // Request for the value of Bid
            double ask   = MarketInfo(strSymbol, MODE_ASK); // Request for the value of Ask
            double point = MarketInfo(strSymbol, MODE_POINT);//Request for Point
            //Calculated SL and TP prices must be normalized                     
            nCurrSellTicket = OrderSend(strSymbol, OP_BUY, 0.1, bid, 50, 0, 0);
            if (nCurrSellTicket == -1)
               Comment("Err = ", GetLastError());
        } //end of IF loss is too high             
    }//End of what we do with the selected current order
    //Now we Check if one of the loosing orders has profit
    for(int j=1;j <= arrSellOrders[0]; j++)
    {
      if (OrderSelect(arrSellOrders[j], SELECT_BY_TICKET) == true)
      {
         dblProfit = OrderProfit() - MathAbs(OrderSwap()) 
                                    - MathAbs(OrderCommission());
         if (dblProfit > dblLevelProfit)
         {
            if (OrderClose(arrSellOrders[j], dblLotSize, Ask, 1, Red) == false)
               Comment("the buy order with profit from losing list could not be closed");
            else
            {
                //adapt the array after selling
                for(int k=j;k < arrSellOrders[0];k++)
                {
                   arrSellOrders[k] = arrSellOrders[k+1]; //shift the orders
                   arrSellOrders[k+1] = -1;
                   arrSellOrders[0]--;
                }
                
            }
            break; //if we found profit we leave the FOR loop
            //If others have profit we will process it at next tick
         }                                              
      }//End of an selected order
      else
         Comment("We could not select order with ticketnr = " + string(arrSellOrders[j])); */        
   }
   Comment("END CHECK");
   return(dblProfit);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
